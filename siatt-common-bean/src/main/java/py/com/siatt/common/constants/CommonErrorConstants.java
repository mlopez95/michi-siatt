package py.com.siatt.common.constants;

public class CommonErrorConstants {
    private CommonErrorConstants() {
        throw new UnsupportedOperationException();
    }

    public static final String ERROR_DESERIALIZE = "cm1011";
    public static final String ERROR_SERIALIZE = "cm1012";
    public static final String ERROR_INVALID_ISO8601_DATE = "cm1020";
    public static final String ERROR_INVALID_DATES = "cm1021";
    public static final String ERROR_INVALID_DATA_FORMAT = "cm1030";
    public static final String ERROR_Siatt_API = "cm1040";
    public static final String ERROR_ESTADO_CLIENTE_INACTIVO = "cm1050";
    public static final String ERROR_ESTADO_CLIENTE_RECHAZADO = "cm1051";
    public static final String ERROR_ESTADO_CLIENTE_PENDIENTE = "cm1052";
    public static final String ERROR_ESTADO_CLIENTE_EXPIRADO = "cm1053";
    public static final String ERROR_INESPERADO = "cm1070";
    public static final String ERROR_DATO_NO_ENCONTRADO = "cm1080";
    public static final String ERROR_PARAMETROS_INCORRECTOS = "cm1380";
    public static final String ERROR_NO_CUENTA_SHARED_SECRET = "cm1400";
    public static final String ERROR_NO_CUENTA_CLAVE_ACCESO = "cm1410";
    public static final String ERROR_ENVIAR_TOKEN = "cm1420";
    public static final String ERROR_NO_TIENE_PERMISO = "cm1430";
    public static final String ERROR_INVALID_CLIENT_VERSION_TYPE = "cm1500";
    public static final String ERROR_INACTIVE_CLIENT_VERSION = "cm1510";

    public static final String ERROR_CLAVE_TRANSACCIONAL_INCORRECTA = "cm1511";
    public static final String ERROR_CLAVE_DINAMICA_INCORRECTA = "cm1512";
    public static final String ERROR_PIN_INCORRECTO = "cm1514";

    public static final String ERROR_SIATT_XX = "d9999";
    public static final String ERROR_SIATT_02 = "d0002";
    public static final String ERROR_SIATT_03 = "d0003";

    public static final String MESSAGE_DESERIALIZE = "Error al deserializar el objeto.";
    public static final String MESSAGE_SERIALIZE = "Error al serializar el objeto.";
    public static final String MESSAGE_SIATT_XX = "Ocurrió un error al solicitar el servicio de Siatt.";
}
