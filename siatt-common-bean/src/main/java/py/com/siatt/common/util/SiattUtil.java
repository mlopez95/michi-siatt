package py.com.siatt.common.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SiattUtil {
    private SiattUtil() {
        throw new UnsupportedOperationException();
    }

    private static Logger logger = LoggerFactory.getLogger(SiattUtil.class);
    private static final int TARJETA_DIGITOS_VISIBLES = 4;
    private static final int TARJETA_DIGITOS_VISIBLES_UN_ASTERISCO = 5;
    private static final String PADDING_CHAR = "*";
    private static final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

    public static java.util.Date getJAVADateFromSQLDate(java.sql.Date sqlDate) {
        java.util.Date javaDate = null;
        if (sqlDate != null) {
            javaDate = new java.util.Date(sqlDate.getTime());
        }
        return javaDate;
    }

    public static java.util.Date getJAVADateFromSQLDate(Long sqlDate) {
        java.util.Date javaDate = null;
        if (sqlDate != null) {
            javaDate = new java.util.Date(sqlDate);
        }
        return javaDate;
    }

    public static java.sql.Timestamp getSQLTimestamp(Date date) {
        java.sql.Timestamp sqlTimestamp = null;
        if (date != null) {
            sqlTimestamp = new java.sql.Timestamp(date.getTime());
        }
        return sqlTimestamp;
    }

    public static String getTimeStamp() {
        Date sysdate = new Date(System.currentTimeMillis());
        return new SimpleDateFormat(ISO8601_FORMAT).format(sysdate);
    }

    public static Date getDate(String dateStr) {
        Date date = null;
        if (dateStr != null) {
            try {
                date = new SimpleDateFormat(ISO8601_FORMAT)
                        .parse(dateStr);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
            }
        }


        return date;
    }

    public static java.sql.Date getSQLDate(String dateStr) {
        Date date = null;
        java.sql.Date sqlDate = null;
        if (dateStr != null) {
            try {
                date = new SimpleDateFormat(ISO8601_FORMAT)
                        .parse(dateStr);
                sqlDate = new java.sql.Date(date.getTime());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
            }
        }


        return sqlDate;
    }

    public static java.util.Date getDateTime(java.util.Date fecha, String hora) {
        try {
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return sdfTime.parse(sdfDate.format(fecha) + " " + hora);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String getDateFormated(java.util.Date fecha, String formato) {
        String fechaStr = null;
        if (fecha != null && formato != null) {
            SimpleDateFormat sdfDate = new SimpleDateFormat(formato);
            fechaStr = sdfDate.format(fecha);

        }
        return fechaStr;

    }

    public static int getMonthFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH) + 1;
    }

    public static int getDayFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static String leftPadding(Long number, String padding, Integer places) {
        String numberPadding = null;
        try {
            String paddingStr = "%" + padding + places + "d";
            numberPadding = String.format(paddingStr, number);
            logger.debug(String.format("Número formateado %s", numberPadding));
        } catch (Exception e) {
            logger.error(String.format("Ocurrió un error al rellenar el número %s con %s %s veces", number, padding, places), e);
        }

        return numberPadding;
    }

    public static String formatearNumeroOperacion(Long numeroOperacion) {
        return leftPadding(numeroOperacion, "0", 10);
    }

    public static boolean isNegative(BigDecimal b) {
        return (b.signum() < 0) ? true : false;
    }

    public static Date lastDay(Date convertedDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(convertedDate);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }

    public static String separarMiles(Number number) {
        Locale l = new Locale("es", "PY");
        DecimalFormat separadorPY = (DecimalFormat) NumberFormat.getNumberInstance(l);
        return separadorPY.format(number);
    }

    public static String getSsign(String text, String sharedSecret, String tstamp) {
        String encode = tstamp + "##" + text + "##" + sharedSecret;
        byte[] str = DigestUtils.sha(encode);
        return new String(Base64.getEncoder().encode(str));
    }
}
