package py.com.siatt.common.constants;

public class DiscoveryConstats {
    private DiscoveryConstats() {
        throw new UnsupportedOperationException();
    }

    public static final String AUTH_API_CODE = "auth-siatt";
    public static final String TARJETAS_API_CODE = "tarjetas-siatt";
    public static final String NOTIFICACIONES_API_CODE = "notificaciones-siatt";
    public static final String TRANSACCIONES_API_CODE = "transacciones-siatt";
    public static final String PAGO_SERVICIOS_API_CODE = "pagoservicios-siatt";
    public static final String FAVORITOS_API_CODE = "favoritos-siatt";
    public static final String CARD_READER_API_CODE = "michi-cardreader";
}
