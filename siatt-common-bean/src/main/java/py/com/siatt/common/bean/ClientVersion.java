package py.com.siatt.common.bean;

import java.util.Map;

/**
 * Created by spezzino on 9/13/17
 */
public class ClientVersion {
    public static final String INACTIVE_CLIENT_VERSION = "Inactivo";

    private String clientType;
    private String clientVersionString;
    private String sharedSecret;
    private Map<String, Object> additionalInfo;
    private String estado;

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientVersionString() {
        return clientVersionString;
    }

    public void setClientVersionString(String clientVersionString) {
        this.clientVersionString = clientVersionString;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    public Map<String, Object> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(Map<String, Object> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
