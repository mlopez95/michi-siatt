package py.com.siatt.common.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.siatt.common.bean.ListaPaginada;

import java.util.Collections;
import java.util.List;

public class ListadoHelper {
    private static Logger logger = LoggerFactory.getLogger(ListadoHelper.class);

    private ListadoHelper() {
        throw new UnsupportedOperationException();
    }

    public static <T> ListaPaginada<T> getListaPaginada(List<T> lista, Integer page, Integer pageSize) {
        logger.info(String.format("se recibe para paginar page=%s y pageSize=%s y lista %s", page, pageSize, lista));
        ListaPaginada<T> listaProcesada = new ListaPaginada<>();
        List<T> listaPaginada = Collections.emptyList();
        listaProcesada.setListaPaginadaList(listaPaginada);
        listaProcesada.setTotalRegistros(0);
        Integer listaSize = null;
        Integer numPages = null;
        if (lista != null && !lista.isEmpty()) {
            listaSize = lista.size();
            listaProcesada.setTotalRegistros(listaSize);
            //si el numero de pagina es null se envia la lista completa
            if (page == null || page == 0) {
                pageSize = listaSize;
                page = 1;
            }
            if (pageSize != null) {
                if (pageSize > listaSize) {
                    pageSize = listaSize;
                }

                numPages = listaSize / pageSize + (listaSize % pageSize == 0 ? 0 : 1);
                listaProcesada.setPaginaActual(page);
                listaProcesada.setTotalPaginas(numPages);
                //si los datos envíados en page y page size son válidos se pagina la lista, sino se envía una lista vacía
                if (numPages >= page && page > 0 && pageSize > 0) {
                    Integer from = (page - 1) * pageSize;//inclusive
                    Integer to = page * pageSize;//exclusive
                    if (to >= listaSize) {
                        to = listaSize;
                    }
                    listaPaginada = lista.subList(from, to);
                    listaProcesada.setListaPaginadaList(listaPaginada);
                }
            }


        }

        return listaProcesada;
    }

}
