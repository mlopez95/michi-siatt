package py.com.siatt.common.bean;

public class Respuesta {
    private Estado estado;
    private String mensaje;

    public Respuesta() {
        // noop
    }

    public Respuesta(Estado estado, String mensaje) {
        super();
        this.estado = estado;
        this.mensaje = mensaje;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "Respuesta [estado=" + estado + ", Mensaje=" + mensaje + "]";
    }


}
