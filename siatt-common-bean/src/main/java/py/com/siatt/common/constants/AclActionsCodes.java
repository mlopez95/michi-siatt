
package py.com.siatt.common.constants;

public class AclActionsCodes {
    private AclActionsCodes() {
        throw new UnsupportedOperationException();
    }

    //auth
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String OBTENER_SESION = "obtener-sesion";

    //usuarios
    public static final String REGISTRAR_USUARIO = "registrar-usuario";
    @SuppressWarnings("squid:S2068")
    public static final String REGISTRAR_PASSWORD = "registrar-password";
    public static final String CONFIRMAR_USUARIO = "confirmar-usuario";
    public static final String REENVIAR_CODIGO_VALIDACION = "reenviar-codigo-validacion";
    @SuppressWarnings("squid:S2068")
    public static final String SOLICITUD_RESET_PASSWORD = "solicitud-reset-password";
    @SuppressWarnings("squid:S2068")
    public static final String VALIDAR_RESET_PASSWORD = "validar-reset-password";
    @SuppressWarnings("squid:S2068")
    public static final String CONFIRMAR_RESET_PASSWORD = "confirmar-reset-password";
    public static final String ACTUALIZAR_PERFIL = "actualizar-perfil";
    public static final String OBTENER_PERFIL = "obtener-perfil";
    @SuppressWarnings("squid:S2068")
    public static final String CAMBIAR_PASSWORD = "cambiar-password";
    public static final String OBTENER_DATOS_USUARIO = "obtener-datos-usuario";

    //notificaciones
    public static final String ENVIAR_EMAIL = "enviar-email";
    public static final String ENVIAR_SMS = "enviar-sms";
    public static final String RECIBIR_NOTIFICACION_BEPSA = "recibir-notificacion-bepsa";
    public static final String LISTAR_CANALES = "listar-canales";
    public static final String LISTAR_TIPOS_NOTIFICACIONES = "listar-tipos-notificaciones";
    public static final String CONSULTAR_CONFIGURACION_NOTIFICACIONES = "consultar-configuracion-notificaciones";
    public static final String ESTABLECER_CONFIGURACION_NOTIFICACIONES = "establecer-configuracion-notificaciones";
    public static final String OBTENER_NOTIFICACIONES = "obtener-notificaciones";
    public static final String MARCAR_NOTIFICACIONES_LEIDAS = "marcar-notificaciones-leidas";
    public static final String ACTUALIZAR_PUSH_TOKEN = "actualizar-pushtoken";

    //tarjetas
    public static final String REGISTRAR_MEDIO_PAGO = "registrar-medio-pago";
    public static final String ELIMINAR_MEDIO_PAGO = "eliminar-medio-pago";
    public static final String EDITAR_MEDIO_PAGO = "editar-medio-pago";
    public static final String OBTENER_MEDIO_PAGO = "obtener-medio-pago";
    public static final String OBTENER_MEDIOS_PAGO = "obtener-medios-pago";
    public static final String CONSULTAR_MOVIMIENTOS_TARJETA = "consultar-movimientos-tarjeta";
    public static final String CONSULTAR_MOVIMIENTOS_TARJETAS = "consultar-movimientos-tarjetas";
    public static final String CONSULTAR_SALDO_DEBITO = "consultar-saldo-debito";
    public static final String REGISTRAR_PAGO_TARJETA = "registrar-pago-tarjeta";
    public static final String CONFIRMAR_PAGO_TARJETA = "confirmar-pago-tarjeta";
    public static final String CONSULTAR_USUARIO_ID = "consultar-usuario-id";
    public static final String CONSULTAR_ESTADO_TARJETA = "consultar-estado-tarjeta";
    public static final String REGISTRAR_ESTADO_TARJETA = "registrar-estado-tarjeta";
    public static final String CONSULTAR_ESCANEO_TARJETA = "consultar-escaneo-tarjeta";
    public static final String CONSULTAR_RESUMEN_MES_ANTERIOR = "consultar-resumen-mes-anterior";

    //pago de servicios
    public static final String REGISTRAR_PAGO_SERVICIOS = "registrar-pago-servicios";
    public static final String CONFIRMAR_PAGO_SERVICIOS = "confirmar-pago-servicios";

    //transacciones
    public static final String REGISTRAR_TRANSACCION = "registrar-transaccion";
    public static final String CONFIRMAR_TRANSACCION = "confirmar-transaccion";
    public static final String CONSULTAR_TRANSACCION = "consultar-transaccion";
    public static final String LISTAR_TRANSACCION = "listar-transaccion";

    //datos generales
    public static final String CONSULTAR_UBICACIONES = "consultar-ubicaciones";
    public static final String CONSULTAR_RUBROS = "consultar-rubros";
    public static final String CONSULTAR_CIUDADES = "consultar-ciudades";

    // favoritos
    public static final String CONSULTAR_FAVORITOS = "consultar-favoritos";
    public static final String EDITAR_FAVORITOS = "editar-favoritos";
    public static final String ELIMINAR_FAVORITOS = "eliminar-favoritos";
    public static final String REGISTRAR_FAVORITOS = "registrar-favoritos";

    public static final String OBTENER_SALUDO = "obtener-saludo";
}

