package py.com.siatt.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.siatt.common.constants.CommonErrorConstants;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class DataUtil {
    private DataUtil() {
        throw new UnsupportedOperationException();
    }

    private static Logger logger = LoggerFactory.getLogger(DataUtil.class);

    public static String serializeList(List<?> listData) {

        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = null;

        try {
            jsonValue = mapper.writeValueAsString(listData);

        } catch (JsonProcessingException e) {
            logger.error("Error de deserializacion", e);
        }
        return jsonValue;
    }

    public static <T> T[] deserializeList(String strListData, Class<T[]> typeClass) {
        ObjectMapper mapper = new ObjectMapper();
        T[] dataFromJson = null;
        try {
            dataFromJson = mapper.readValue(strListData, typeClass);
        } catch (IOException e) {
            logger.info(String.format("%s %s: %s", CommonErrorConstants.MESSAGE_DESERIALIZE, typeClass, e.getMessage()));
        }
        return dataFromJson;
    }

    public static <T> T deserializeJsonToObject(String content, Class<T> typeClass) {
        ObjectMapper mapper = new ObjectMapper();
        T dataFromJson = null;
        try {
            dataFromJson = mapper.readValue(content, typeClass);
        } catch (IOException e) {
            logger.info(String.format("%s %s: %s", CommonErrorConstants.MESSAGE_DESERIALIZE, typeClass, e.getMessage()));
        }
        return dataFromJson;
    }

    public static <T> String serializeObjectToJson(T object) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonvalue = null;
        try {
            jsonvalue = mapper.writeValueAsString(object);
        } catch (IOException e) {
            logger.info(String.format("%s: %s ", CommonErrorConstants.MESSAGE_SERIALIZE, e.getMessage()));
        }
        return jsonvalue;
    }

    public static <T> List<T> deserializeListToLIST(String strListData, Class<T[]> typeClass) {
        ObjectMapper mapper = new ObjectMapper();
        T[] dataFromJson = null;
        try {
            dataFromJson = mapper.readValue(strListData, typeClass);
        } catch (IOException e) {
            logger.info(String.format("No se pudo deserializar el tipo lista de %s: %s", typeClass, e));
        }


        return Arrays.asList(dataFromJson);
    }


}
