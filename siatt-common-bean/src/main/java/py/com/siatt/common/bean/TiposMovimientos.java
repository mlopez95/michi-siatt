package py.com.siatt.common.bean;
public enum TiposMovimientos {
	TODOS(""), DEBITO("D"), CREDITO("C");

	private String codigo;

	private TiposMovimientos(String codigo) {
		this.codigo = codigo;
	}
	
	public static TiposMovimientos getTiposMovimientosFromString(String tipoMovimiento){
	    for (TiposMovimientos o : TiposMovimientos.values()) {	    	
			if(o.codigo.equalsIgnoreCase(tipoMovimiento != null ? tipoMovimiento : "")){
				return o;
			}
		}
		return null;
	}

	public String getCodigo() {
		return codigo;
	}
}