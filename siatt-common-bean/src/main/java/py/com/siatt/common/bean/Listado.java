package py.com.siatt.common.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Contiene toda la información para realizar paginación, filtros y busquedas")
public class Listado {
    @ApiModelProperty(value = "Total de items del listado")
    private Integer totalRegistros;
    @ApiModelProperty(value = "Página actual")
    private Integer paginaActual;
    @ApiModelProperty(value = "Total de páginas")
    private Integer totalPaginas;

    public Listado(Integer totalRegistros, Integer paginaActual, Integer totalPaginas) {
        super();
        this.totalRegistros = totalRegistros;
        this.paginaActual = paginaActual;
        this.totalPaginas = totalPaginas;
    }

    public Listado() {
        // noop
    }

    public Integer getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(Integer totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public Integer getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(Integer paginaActual) {
        this.paginaActual = paginaActual;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    @Override
    public String toString() {
        return "Listado [totalRegistros=" + totalRegistros + ", paginaActual=" + paginaActual + ", totalPaginas="
                + totalPaginas + "]";
    }


}
