package py.com.siatt.common.bean;

import java.util.List;

public class ListaPaginada<T> extends Listado {

    private List<T> listaPaginadaList;

    public ListaPaginada() {
        // noop
    }

    public List<T> getListaPaginadaList() {
        return listaPaginadaList;
    }

    public void setListaPaginadaList(List<T> listaPaginadaList) {
        this.listaPaginadaList = listaPaginadaList;
    }

    @Override
    public String toString() {
        return "ListaPaginada [listaPaginadaList=" + listaPaginadaList + ", getTotalRegistros()=" + getTotalRegistros()
                + ", getPaginaActual()=" + getPaginaActual() + ", getTotalPaginas()=" + getTotalPaginas() + "]";
    }


}
