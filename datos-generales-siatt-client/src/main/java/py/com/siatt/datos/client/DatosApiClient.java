package py.com.siatt.datos.client;

import com.roshka.michi.bean.TrackingInfo;
import com.roshka.michi.client.MichiBaseClient;

public class DatosApiClient extends MichiBaseClient {

    public DatosApiClient(TrackingInfo trackingInfo) {
        super(trackingInfo);
    }
}
