package py.com.siatt.datos.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(description = "Datos de ubicacion")
public class Mensaje {

    @ApiModelProperty(value = "Nombre de Prueba",example = "Marcelo")
    private String name;

    @ApiModelProperty(value = "Mensaje de prueba",example = "Hola fulano, como estas ?")
    private String mensaje;

    @Override
    public String toString() {
        return "Ubicacion{" +
                "name='" + name + '\'' +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
