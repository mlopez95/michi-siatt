package py.com.siatt.common.util;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by spezzino on 8/30/17
 */
@Aspect
@Component
public class MethodLogger {
    private Logger logger = LoggerFactory.getLogger(MethodLogger.class);

    @Around("execution(* *(..)) && @annotation(py.com.siatt.common.annotation.Loggable)")
    public Object aroundController(ProceedingJoinPoint point) throws Throwable {
        long start = System.currentTimeMillis();
        Object result;
        result = point.proceed();

        Map args = zipToMap(Arrays.asList(MethodSignature.class.cast(point.getSignature()).getParameterNames()), Arrays.asList(point.getArgs()));

        logger.info(String.format(
                "%s#%s(%s): %s (%sms)",
                MethodSignature.class.cast(point.getSignature()).getMethod().getDeclaringClass().getName(),
                MethodSignature.class.cast(point.getSignature()).getMethod().getName(),
                args,
                result,
                System.currentTimeMillis() - start
        ));

        return result;
    }

    private static <K, V> Map<K, V> zipToMap(List<K> keys, List<V> values) {
        Iterator<K> keyIter = keys.iterator();
        Iterator<V> valIter = values.iterator();
        Map<K, V> map = new LinkedHashMap<>();
        while (keyIter.hasNext()) {
            K key = keyIter.next();
            V value = valIter.next();
            map.put(key, value);
        }
        return map;
    }
}
