package py.com.siatt.common.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.roshka.michi.dao.DatabagDAOImpl;
import com.roshka.michi.exception.APIException;
import com.roshka.michi.exception.APIExceptionType;
import com.roshka.michi.util.DBMichiTablesNames;
import com.roshka.michi.util.DBUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import py.com.siatt.common.bean.ClientVersion;
import py.com.siatt.common.constants.CommonErrorConstants;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by spezzino on 9/13/17
 */
@Repository
public class ClientVersionDAOImpl extends JdbcDaoSupport implements ClientVersionDAO {

    private static final String TABLE_NAME = DBMichiTablesNames.CV;
    private static final String TABLE_ALIAS = "client_versions";
    private static final String TABLE_FROM = TABLE_NAME + " " + TABLE_ALIAS;

    private static final String[] FIELDS = {
            "client_type",
            "client_version",
            "shared_secret",
            "additional_info",
            "estado"
    };

    private static final String SQL_SELECT_BASE =
            "select " + DBUtil.getSelectClauseFields(TABLE_ALIAS, FIELDS) + ", " +
                    DBUtil.getSelectClauseFields(DatabagDAOImpl.TABLE_ALIAS, DatabagDAOImpl.FIELDS) +
                    " from " + TABLE_FROM + " left outer join " + DatabagDAOImpl.TABLE_NAME + " databag on (" + TABLE_ALIAS + ".additional_info = databag.databag_iid and databag.databag_type = 'AUTH.CLIENT_VERSIONS/ADDITIONAL_INFO') ";
    private static final String SQL_SELECT_BY_CLIENT_TYPE_VERSION = SQL_SELECT_BASE + " where " + TABLE_ALIAS + ".client_type = ? and " + TABLE_ALIAS + ".client_version = ?";

    @Autowired
    public ClientVersionDAOImpl(DataSource apiDataSource) {
        setDataSource(apiDataSource);
    }

    private class ClientVersionMapper implements RowMapper<ClientVersion> {
        @SuppressWarnings("unchecked")
        public ClientVersion mapRow(ResultSet rs, int rowNum) throws SQLException {
            ClientVersion cv = new ClientVersion();
            cv.setClientType(rs.getString("client_type"));
            cv.setClientVersionString(rs.getString("client_version"));
            cv.setSharedSecret(rs.getString("shared_secret"));
            long additionalInfo = rs.getLong("additional_info");
            cv.setEstado(rs.getString("estado"));
            if (!rs.wasNull() && additionalInfo != 0L) {
                String databagValue = rs.getString("databag_val");
                try {
                    cv.setAdditionalInfo(new ObjectMapper().readValue(databagValue, HashMap.class));
                } catch (Exception e) {
                    logger.error("Can't get deserialize databag", e);
                }
            }
            return cv;
        }
    }

    @Override
    public ClientVersion getClientVersion(String clientType, String clientVersion) throws APIException {
        logger.info(String.format("Getting ClientVersion from clientType [%s] and clientVersion [%s]", clientType, clientVersion));
        try {
            return getJdbcTemplate().
                    queryForObject(SQL_SELECT_BY_CLIENT_TYPE_VERSION, new ClientVersionMapper(), clientType, clientVersion);
        } catch (EmptyResultDataAccessException e) {
            throw new APIException(APIExceptionType.APPLICATION, CommonErrorConstants.ERROR_INVALID_CLIENT_VERSION_TYPE, String.format("Invalid values for client type [%s] and client version [%s]", clientType, clientVersion));
        }
    }
}
