package py.com.siatt.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roshka.michi.bean.MichiContext;
import com.roshka.michi.exception.APIException;
import com.roshka.michi.exception.APIExceptionType;
import py.com.siatt.common.constants.CommonErrorConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ContextUtil {
    private ContextUtil() {
        throw new UnsupportedOperationException();
    }

    public static String getInfoContextotToJson(MichiContext ctx, String origen, String ipAddress, String timestamp) throws APIException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> infoContexto = new HashMap<>();
        infoContexto.put("tracking_id", ctx.getTrackingInfo().getTrackingNo());
        infoContexto.put("call_id", ctx.getTrackingInfo().getCallId());
        infoContexto.put("ip_acceso", ipAddress);
        infoContexto.put("origen", origen);
        infoContexto.put("timestamp", timestamp);

        String infoContextoJson = "";
        try {
            infoContextoJson = mapper.writeValueAsString(infoContexto);
        } catch (JsonProcessingException e) {
            throw new APIException(APIExceptionType.INTERNAL, CommonErrorConstants.ERROR_SERIALIZE,
                    CommonErrorConstants.MESSAGE_SERIALIZE, e);
        }
        return infoContextoJson;
    }

    public static String getInfoUsuarioToJson(String codPersona, String token, String ssing) throws APIException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> infoUsuario = new HashMap<>();
        infoUsuario.put("cod_persona", codPersona);
        infoUsuario.put("token", token);
        infoUsuario.put("transaccional", ssing);

        String infoUsuarioJson = "";
        try {
            infoUsuarioJson = mapper.writeValueAsString(infoUsuario);
        } catch (JsonProcessingException e) {
            throw new APIException(APIExceptionType.INTERNAL, CommonErrorConstants.ERROR_SERIALIZE, CommonErrorConstants.MESSAGE_SERIALIZE, e);
        }
        return infoUsuarioJson;
    }

    public static String getInfoUsuarioToJsonLogin(String documento, String documentoEmpresa, String tipoAuth) throws APIException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> infoUsuario = new HashMap<>();
        infoUsuario.put("nro_doc", documento);
        infoUsuario.put("nro_docj", documentoEmpresa);
        infoUsuario.put("tipo_aut", tipoAuth);

        String infoUsuarioJson = "";
        try {
            infoUsuarioJson = mapper.writeValueAsString(infoUsuario);
        } catch (JsonProcessingException e) {
            throw new APIException(APIExceptionType.INTERNAL, CommonErrorConstants.ERROR_SERIALIZE, CommonErrorConstants.MESSAGE_SERIALIZE, e);
        }
        return infoUsuarioJson;
    }

    public static String getInfoUsuarioToCodPersonaJson(String documento, String tipoAuth, String email) throws APIException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> infoUsuario = new HashMap<>();
        infoUsuario.put("nro_doc", documento);
        infoUsuario.put("tipo_dato", tipoAuth);
        infoUsuario.put("email", email);

        String infoUsuarioJson = "";
        try {
            infoUsuarioJson = mapper.writeValueAsString(infoUsuario);
        } catch (JsonProcessingException e) {
            throw new APIException(APIExceptionType.INTERNAL, CommonErrorConstants.ERROR_SERIALIZE, "Can't serialize map", e);
        }
        return infoUsuarioJson;
    }

    public static String getInfoContextotToJson(MichiContext ctx, String origen, String ipAddress, String timestamp, String contratoCambio) throws APIException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> infoContexto = new HashMap<>();
        infoContexto.put("tracking_id", ctx.getTrackingInfo().getTrackingNo());
        infoContexto.put("call_id", ctx.getTrackingInfo().getCallId());
        infoContexto.put("ip_acceso", ipAddress);
        infoContexto.put("origen", origen);
        infoContexto.put("timestamp", timestamp);
        infoContexto.put("nro_con_cambio", contratoCambio);

        String infoContextoJson = "";
        try {
            infoContextoJson = mapper.writeValueAsString(infoContexto);
        } catch (JsonProcessingException e) {
            throw new APIException(APIExceptionType.INTERNAL, CommonErrorConstants.ERROR_SERIALIZE,
                    "Can't serialize map", e);
        }
        return infoContextoJson;
    }

    public static String getIp(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }
}
