package py.com.siatt.common.dao;

import com.roshka.michi.exception.APIException;
import py.com.siatt.common.bean.ClientVersion;

/**
 * Created by spezzino on 9/13/17
 */
public interface ClientVersionDAO {

    ClientVersion getClientVersion(String clientType, String clientVersion) throws APIException;

}
