/*
package py.com.siatt.common.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "file:${michi.server.config.dir}siatt-init.properties", encoding = "UTF-8")
public class PropertiesReader {

    @Value("${notificaciones.siatt.email.validacion}")
    private String emailValidacion;

    @Value("${notificaciones.siatt.email.asunto}")
    private String emailAsunto;

    @Value("${notificaciones.siatt.sms.validacion}")
    private String smsValidacion;

    @Value("${auth.sesion.duracion}")
    private Integer duracionSesion;

    @Value("${auth.sesion.duplicada.msj}")
    private String msjSesionDuplicada;

    @Value("${siatt.ws.apikey}")
    private String siattWSApiKey;

    @Value("${siatt.ws.url}")
    private String siattWSUrl;

    @Value("${siatt.activation.link.base}")
    private String siattActivationLinkBase;

    @Value("${siatt.activation.logo}")
    private String siattActivationLogo;

    @Value("${fcm.push.endpoint}")
    private String fcmPushEndpoint;

    @Value("${fcm.auth.key}")
    private String fcmAuthKey;

    @Value("${api.pds.apikey}")
    private String pdsApiKey;

    @Value("${api.pds.endpoint}")
    private String pdsEndpoint;

    @Value("${auth.security.maxFailedLogins}")
    private Integer maxFailedLogins;

    @Value("${auth.security.blockLoginTime}")
    private Integer blockLoginTime;

    @Value("${auth.security.multipleSession}")
    private Boolean multipleSession;

    public String getEmailValidacion() {
        return emailValidacion;
    }

    public void setEmailValidacion(String emailValidacion) {
        this.emailValidacion = emailValidacion;
    }

    public String getEmailAsunto() {
        return emailAsunto;
    }

    public void setEmailAsunto(String emailAsunto) {
        this.emailAsunto = emailAsunto;
    }

    public String getSmsValidacion() {
        return smsValidacion;
    }

    public void setSmsValidacion(String smsValidacion) {
        this.smsValidacion = smsValidacion;
    }

    public Integer getDuracionSesion() {
        return duracionSesion;
    }

    public void setDuracionSesion(Integer duracionSesion) {
        this.duracionSesion = duracionSesion;
    }

    public String getMsjSesionDuplicada() {
        return msjSesionDuplicada;
    }

    public void setMsjSesionDuplicada(String msjSesionDuplicada) {
        this.msjSesionDuplicada = msjSesionDuplicada;
    }

    public String getSiattWSApiKey() {
        return siattWSApiKey;
    }

    public void setSiattWSApiKey(String siattWSApiKey) {
        this.siattWSApiKey = siattWSApiKey;
    }

    public String getSiattWSUrl() {
        return siattWSUrl;
    }

    public void setSiattWSUrl(String siattWSUrl) {
        this.siattWSUrl = siattWSUrl;
    }

    public String getSiattActivationLinkBase() {
        return siattActivationLinkBase;
    }

    public void setSiattActivationLinkBase(String siattActivationLinkBase) {
        this.siattActivationLinkBase = siattActivationLinkBase;
    }

    public String getSiattActivationLogo() {
        return siattActivationLogo;
    }

    public void setSiattActivationLogo(String siattActivationLogo) {
        this.siattActivationLogo = siattActivationLogo;
    }

    public String getFcmPushEndpoint() {
        return fcmPushEndpoint;
    }

    public void setFcmPushEndpoint(String fcmPushEndpoint) {
        this.fcmPushEndpoint = fcmPushEndpoint;
    }

    public String getFcmAuthKey() {
        return fcmAuthKey;
    }

    public void setFcmAuthKey(String fcmAuthKey) {
        this.fcmAuthKey = fcmAuthKey;
    }

    public String getPdsApiKey() {
        return pdsApiKey;
    }

    public void setPdsApiKey(String pdsApiKey) {
        this.pdsApiKey = pdsApiKey;
    }

    public String getPdsEndpoint() {
        return pdsEndpoint;
    }

    public void setPdsEndpoint(String pdsEndpoint) {
        this.pdsEndpoint = pdsEndpoint;
    }

    public Integer getMaxFailedLogins() {
        return maxFailedLogins;
    }

    public void setMaxFailedLogins(Integer maxFailedLogins) {
        this.maxFailedLogins = maxFailedLogins;
    }

    public Integer getBlockLoginTime() {
        return blockLoginTime;
    }

    public void setBlockLoginTime(Integer blockLoginTime) {
        this.blockLoginTime = blockLoginTime;
    }

    public Boolean getMultipleSession() {
        return multipleSession;
    }

    public void setMultipleSession(Boolean multipleSession) {
        this.multipleSession = multipleSession;
    }

    @Override
    public String toString() {
        return "PropertiesReader [" +
                "emailValidacion='" + emailValidacion + '\'' +
                ", emailAsunto='" + emailAsunto + '\'' +
                ", smsValidacion='" + smsValidacion + '\'' +
                ", duracionSesion=" + duracionSesion +
                ", msjSesionDuplicada='" + msjSesionDuplicada + '\'' +
                ", siattWSApiKey='" + siattWSApiKey + '\'' +
                ", siattWSUrl='" + siattWSUrl + '\'' +
                ", siattActivationLinkBase='" + siattActivationLinkBase + '\'' +
                ", siattActivationLogo='" + siattActivationLogo + '\'' +
                ", fcmPushEndpoint='" + fcmPushEndpoint + '\'' +
                ", fcmAuthKey='" + fcmAuthKey + '\'' +
                ", pdsApiKey='" + pdsApiKey + '\'' +
                ", pdsEndpoint='" + pdsEndpoint + '\'' +
                ", maxFailedLogins=" + maxFailedLogins +
                ", blockLoginTime=" + blockLoginTime +
                ", multipleSession=" + multipleSession +
                ']';
    }
}*/
