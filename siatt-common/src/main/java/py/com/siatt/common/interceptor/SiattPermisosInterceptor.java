package py.com.siatt.common.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.roshka.michi.annotation.AclAction;
import com.roshka.michi.bean.MichiContext;
import com.roshka.michi.bean.ServerInfo;
import com.roshka.michi.client.GenericApiClient;
import com.roshka.michi.communication.Request;
import com.roshka.michi.communication.Request.Method;
import com.roshka.michi.constants.MichiConstants;
import com.roshka.michi.constants.MichiErrorConstants;
import com.roshka.michi.exception.APIClientException;
import com.roshka.michi.exception.APIException;
import com.roshka.michi.exception.APIExceptionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import py.com.siatt.common.constants.CommonErrorConstants;
import py.com.siatt.common.constants.DiscoveryConstats;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class SiattPermisosInterceptor extends HandlerInterceptorAdapter {


    private Logger logger = LoggerFactory.getLogger(SiattPermisosInterceptor.class);

    @Autowired
    private ServerInfo serverInfo;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        logger.info("Ejecutando el Siatt Permisos interceptor: " + request.getRequestURI());

        MichiContext mctx = MichiContext.getContext(request);

        APIException apiEx = null;

        try {

            //annotations
            HandlerMethod method = (HandlerMethod) handler;
            AclAction aclAction = AnnotationUtils.findAnnotation(method.getMethod(), AclAction.class);
            if (aclAction == null || aclAction.actionCode() == null || aclAction.actionCode().length <= 0) {
                throw new APIException(APIExceptionType.SECURITY, MichiErrorConstants.ERROR_SEC_ACTION_UNDEFINED, String.format("El método [%s] no tiene la anotación @AclAction", method.getMethod().getName()));
            }
            String[] actions = aclAction.actionCode();
            Boolean apiKeyClient = false;
            //se busca si el apikey es de alguno de los clientes
            //TODO usar MICHI-ACL
//            for (SiattApi e : SiattApi.values()) {
//                if (e.getApiKey().equals(mctx.getCallingApiKey())) {
//                    apiKeyClient = true;
//                }
//            }
            logger.info(String.format("Canal de origen %s", apiKeyClient));
            //sólo si la llamada proviene de alguno de los clientes registrados
            if (apiKeyClient) {
                //obtenemos los permisos
                List<String> permisosCliente = getPermisosSession(mctx);
                if (permisosCliente == null || permisosCliente.isEmpty()) {
                    logger.info("Lista de permisos es null o empty ");
                    throw new APIException(APIExceptionType.SECURITY, CommonErrorConstants.ERROR_NO_TIENE_PERMISO, "No posee permisos para realizar la operacion", true);
                }

                logger.info(String.format("Verificando si el usuario tiene permmisos para %s", actions));
                boolean tienePermiso = false;
                //se recorre todos los permisos asociados al controller
                //TODO verificar cuando el acl esté terminado
                for (String action : actions) {


                    //se verifica si posee el permiso
                    tienePermiso = permisosCliente.contains(action);
                    if (tienePermiso) {
                        break;
                    }
                }


                if (!tienePermiso) {
                    apiEx = new APIException(APIExceptionType.SECURITY, CommonErrorConstants.ERROR_NO_TIENE_PERMISO, "No posee permisos para realizar la operacion", true);
                }
            } else {
                logger.info("No se verifican permisos para el canal de origen %s");
            }

        } catch (APIException e) {
            apiEx = e;
        }

        request.setAttribute(MichiConstants.ATTRIBUTE_CONTEXT, mctx);
        //request.setAttribute(MichiConstants.ATTRIBUTE_LOGENTRY_FUTURE, logRequestStartEntry);

        if (apiEx != null) {
            //llamamos explicitamente al afterCompletion ya que si aqui en el preHandle se lanza una ex el afterCompletion ya no se invoca
            afterCompletion(request, response, handler, apiEx);
            throw apiEx;
        }


        return true;
    }


    private List<String> getPermisosSession(MichiContext mctx) throws APIException {

        GenericApiClient rest = new GenericApiClient(mctx.getTrackingInfo());

        String command = "session";

        Request request = new Request(mctx.getTrackingInfo());
        request.setMethod(Method.GET)
                .addHeader(MichiConstants.HEADER_API_KEY, mctx.getTrackingInfo().getApiKey())
                .addPathParam(0, mctx.getAccessToken());

        try {

            //String sessionJson = rest.makeRequest(request, DiscoveryConstats.AUTH_API_CODE, command, Map.class);
            Map<String, Object> sessionJson = rest.makeRequest(request, DiscoveryConstats.AUTH_API_CODE, command, Map.class);
            ObjectMapper mapper = new ObjectMapper();
            //JsonNode o = mapper.readTree(sessionJson);
            //JsonNode permisos = o.get("permisos");
            List<String> listadoPermisos = (List<String>) sessionJson.getOrDefault("permisos", "{}");

			/*JsonNode permisos =mapper.readTree(permisosStr!= null ? permisosStr : "{}");
            List<String> listadoPermisos = Lists.newArrayList(permisos.iterator()).stream().map(JsonNode::asText).collect(Collectors.toList());*/
            return listadoPermisos;
        } catch (APIClientException e) {
            logger.info("Ocurrió un error al obtener la sesion", e);
            throw new APIException(e);
        }

    }

    public static void main(String[] args) {
        String a = new String("Consultar");
        String b = new String("Consultar");
        System.out.println(a.equals(b));
    }
}
