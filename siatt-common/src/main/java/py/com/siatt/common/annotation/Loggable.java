package py.com.siatt.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by spezzino on 8/30/17
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Loggable {
}
