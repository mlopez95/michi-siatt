package py.com.siatt.common.dao;

import java.util.Date;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.roshka.michi.dao.GlobalDAO;
import com.roshka.michi.exception.APIException;

@Repository
public class GlobalDAOImpl extends JdbcDaoSupport implements GlobalDAO {

    @Autowired
    public GlobalDAOImpl(DataSource apiDataSource) {
        setDataSource(apiDataSource);
    }

    @Override
    public Date getGlobalDate() throws APIException {
        return getJdbcTemplate().queryForObject("SELECT current date FROM sysibm.sysdummy1", Date.class);
    }

    @Override
    public Date getGlobalDateTime() throws APIException {
        try {
            return getJdbcTemplate().queryForObject("SELECT current timestamp FROM sysibm.sysdummy1", Date.class);
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

}
