package py.com.siatt.datos.service;

import com.roshka.michi.exception.APIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import py.com.siatt.datos.bean.Mensaje;
import py.com.siatt.datos.dao.DatosDAO;


@Service
public class DatosServiceImpl implements DatosService {
    @Autowired
    DatosDAO datosDAO;

    @Override
    public Mensaje obtenerSaludo(String nombre) throws APIException {
        return datosDAO.obtenerSaludo(nombre);
    }



/*    @Override
    @Cacheable(cacheManager = "siattCacheManager", cacheNames = "obtenerRubros")
    public RespuestaRubro obtenerRubros() throws APIException {
        RespuestaRubro respuestaRubro = new RespuestaRubro();
        List<Rubro> listaRubros = datosDAO.obtenerRubros();

        ListaPaginada<Rubro> paginacion = ListadoHelper.getListaPaginada(listaRubros, null, null);

        respuestaRubro.setListaRubros(paginacion.getListaPaginadaList());
        respuestaRubro.setPaginaActual(paginacion.getPaginaActual());
        respuestaRubro.setTotalPaginas(paginacion.getTotalPaginas());
        respuestaRubro.setTotalRegistros(paginacion.getTotalRegistros());

        return respuestaRubro;
    }*/

}
