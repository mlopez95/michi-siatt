package py.com.siatt.datos.dao;

import com.roshka.michi.exception.APIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import py.com.siatt.datos.bean.Mensaje;

import javax.sql.DataSource;

@Repository
public class DatosDAOImpl extends JdbcDaoSupport implements DatosDAO {

    @Override
    public Mensaje obtenerSaludo(String name) throws APIException {
        Mensaje mensaje = new Mensaje();
        mensaje.setName(name);
        mensaje.setMensaje("Hola "+name+", como estas ?");
        return mensaje;
    }

    @Autowired
    public DatosDAOImpl(DataSource apiDataSource) {
        setDataSource(apiDataSource);
    }

    /*@Override
    @Cacheable(cacheManager = "siattCacheManager", cacheNames = "obtenerUbicaciones")
    public List<Ubicacion> obtenerUbicaciones(String rubroId, String agrupador, String denominacion, String direccion) throws APIException {
        logger.info(String.format("Vamos a CONSULTAR todos los registro de la tabla M_UBICACIONES: %s", SQL_SELECT_UBICACIONES));
        logger.info(String.format("Parámetros: 1:%s, 2:%s, 3:%s, 4:%s", rubroId, agrupador, denominacion, direccion));


        Object[] params = {rubroId, rubroId,
                agrupador, agrupador,
                denominacion != null ? "%" + denominacion + "%" : null, denominacion != null ? "%" + denominacion + "%" : null,
                direccion != null ? "%" + direccion + "%" : null, direccion != null ? "%" + direccion + "%" : null};
        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

        try {
            List<Ubicacion> list = (List<Ubicacion>) getJdbcTemplate().query(SQL_SELECT_UBICACIONES, params, types, new UbicacionResultSetExtractor());
            logger.info(String.format("Se recuperaron %s registros", list.size()));
            return list;
        } catch (EmptyResultDataAccessException e) {
            logger.info("Se recuperaron 0 registros");
            return new ArrayList<Ubicacion>();
        }
    }*/

}
