package py.com.siatt.datos.service;

import com.roshka.michi.exception.APIException;
import py.com.siatt.datos.bean.Mensaje;

public interface DatosService {

    Mensaje obtenerSaludo(String nombre) throws APIException;
}
