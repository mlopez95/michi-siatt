package py.com.siatt.datos.controller;

import com.roshka.michi.annotation.AclAction;
import com.roshka.michi.bean.MichiContext;
import com.roshka.michi.exception.APIException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import py.com.siatt.common.annotation.Loggable;
import py.com.siatt.common.constants.AclActionsCodes;
import py.com.siatt.datos.bean.Mensaje;
import py.com.siatt.datos.service.DatosService;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(value = "DatosApi", tags = "Datos")
public class DatosAPI {
    @Autowired
    DatosService datosServices;

    @Autowired
    private HttpServletRequest request;

    @AclAction(actionCode = AclActionsCodes.OBTENER_SALUDO)
    @ApiOperation(value = "Obtiene un mensaje", notes = "Realiza la consulta de un mensaje.", nickname = "obtenerSaludo")
    @RequestMapping(value = "/saludo", method = RequestMethod.GET)
    @Loggable
    public Mensaje obtenerSaludo(
            @ApiParam(name = "nombre", value = "Nombre del usuario que quiere recibir el saludo")
            @RequestParam(value = "nombre", required = false) String nombre

    ) throws APIException {
        MichiContext ctx = MichiContext.getContext(request, false);
        return datosServices.obtenerSaludo(nombre);
    }

}
