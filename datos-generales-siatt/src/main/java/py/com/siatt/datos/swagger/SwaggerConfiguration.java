package py.com.siatt.datos.swagger;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.roshka.michi.bean.ErrorBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@EnableSwagger2
public class SwaggerConfiguration {

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket api() {
        //new ModelRef(type)
        List<ResponseMessage> errors = new ArrayList<ResponseMessage>();
        errors.add(new ResponseMessageBuilder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message("Invalid Request")
                .responseModel(new ModelRef("ErrorBody"))
                .build()
        );
        errors.add(new ResponseMessageBuilder()
                .code(HttpStatus.UNAUTHORIZED.value())
                .message("Security exception")
                .responseModel(new ModelRef("ErrorBody"))
                .build()
        );
        errors.add(new ResponseMessageBuilder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message("Unexpected server exception")
                .responseModel(new ModelRef("ErrorBody"))
                .build()
        );

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(
                        Lists.newArrayList(new ParameterBuilder()
                                .name("X-RshkMichi-ApiKey")
                                .description("API KEY de la aplicación solicitante.")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .required(true)
                                .build()))
                .produces(Sets.newHashSet("application/json", "application/xml"))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, errors)
                .globalResponseMessage(RequestMethod.POST, errors)
                .globalResponseMessage(RequestMethod.PUT, errors)
                .globalResponseMessage(RequestMethod.DELETE, errors)
                .additionalModels(typeResolver.resolve(ErrorBody.class))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "Datos Generales Siatt",
                "API que provee datos generales",
                "v" + getClass().getPackage().getImplementationVersion(),
                "",
                new Contact("Roshka", "www.roshka.com", "spezzino@roshka.com"),
                "",
                "");
        return apiInfo;
    }
}
