package py.com.siatt.datos.dao;


import com.roshka.michi.exception.APIException;
import py.com.siatt.datos.bean.Mensaje;

public interface DatosDAO {

    Mensaje obtenerSaludo(String name) throws APIException;
}
