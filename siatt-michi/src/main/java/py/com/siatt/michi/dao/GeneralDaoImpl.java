package py.com.siatt.michi.dao;

import com.roshka.michi.constants.MichiErrorConstants;
import com.roshka.michi.exception.APIException;
import com.roshka.michi.exception.APIExceptionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import py.com.siatt.michi.beans.MensajeSistema;
import py.com.siatt.michi.beans.MensajeSistema.TipoMensaje;
import py.com.siatt.michi.beans.ServiceApi;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Repository
public class GeneralDaoImpl extends JdbcDaoSupport {

    private static final String SQL_GET_MENSAJES =
            "SELECT * FROM public.system_messages " +
                    "WHERE estado = 'ACTIVO' " +
                    "AND (client_type IS NULL OR client_type=?) " +
                    "AND (client_version IS NULL OR client_version=?) " +
                    "AND (hora_desde_activo IS NULL OR CURRENT_TIME >= hora_desde_activo) " +
                    "AND (hora_hasta_activo IS NULL OR CURRENT_TIME <= hora_hasta_activo)";

    private static final String SQL_GET_SERVICES = "SELECT s.api_code , r.url, s.dominio" +
            " FROM MICHI_SERVICES s" +
            " LEFT JOIN MICHI_API_REGISTER r" +
            " ON s.API_CODE = r.API_CODE AND s.API_VERSION = r.API_VERSION" +
            " WHERE s.ESTADO='ACTIVO' AND s.CLIENT_TYPE = ? AND s.CLIENT_VERSION = ?";

    @Autowired
    public GeneralDaoImpl(DataSource generalDataSource) {
        setDataSource(generalDataSource);
    }

    /**
     * Los mensajes son los que esten en estado Activo, Si el client_type es null es para todos los CT, si tiene seteado es solo para ese,
     * Si el client_version es null es para todos los CV,  si tiene seteado es solo para ese
     *
     * @param clientType
     * @param clientVersion
     * @return
     */

    public List<MensajeSistema> obtenerMensajes(String clientType, String clientVersion) {
        logger.info(String.format("Vamos a obtener los mensajes para el clientType %s y el clientVersion %s", clientType, clientVersion));
        try {

            return getJdbcTemplate().query(SQL_GET_MENSAJES, new Object[]{clientType, clientVersion}, new MensajeSistemaMapper());

        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al obtener los mensajes ", e);
            //mandamos un listado vacio
            return new ArrayList<>();
        }
    }

    public List<ServiceApi> obtenerApis(String clientType, String clientVersion) throws APIException {
        logger.info(String.format("Vamos a obtener los servicios para el clientType %s y el clientVersion %s", clientType, clientVersion));
        try {

            return getJdbcTemplate().query(SQL_GET_SERVICES, new Object[]{clientType, clientVersion}, new ServicesMapper());

        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al obtener los apis ", e);
            throw new APIException(APIExceptionType.DATABASE, MichiErrorConstants.ERROR_UNEXPECTED_DB);
        }
    }

    private class MensajeSistemaMapper implements RowMapper<MensajeSistema> {

        @Override
        public MensajeSistema mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajeSistema mensaje = new MensajeSistema();
            mensaje.setCodigo(rs.getString("cod"));
            mensaje.setTipo(TipoMensaje.valueOf(rs.getString("tipo")));
            mensaje.setMensaje(rs.getString("mensaje"));
            return mensaje;
        }
    }

    private class ServicesMapper implements RowMapper<ServiceApi> {

        @Override
        public ServiceApi mapRow(ResultSet rs, int rowNum) throws SQLException {
            ServiceApi service = new ServiceApi();
            service.setCodigo(rs.getString("api_code"));

            String url = rs.getString("url");//la url del discovery con ip interna
            String dominio = rs.getString("dominio"); //el dominio
            if (dominio != null && url != null) {
                //hay dominio hay que concatenar
                try {
                    URI uri = new URI(url);
                    //la url final
                    url = dominio + uri.getPath();
                } catch (URISyntaxException e) {
                    //sera null
                    url = null;
                }
            }

            service.setEndpoint(url);
            return service;
        }

    }
}
