package py.com.siatt.michi.beans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ServiceApi {

	@ApiModelProperty(value="Codigo del api")
	private String codigo;
	@ApiModelProperty(value="URL donde se encuentra el api")
	private String endpoint;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
	
}
