package py.com.siatt.michi.beans;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class DatosIniciales {

	@ApiModelProperty(value="Los apis a utilizar")
	private List<ServiceApi> apis;
	@ApiModelProperty(value="Los mensajes a mostrar")
	private List<MensajeSistema> mensajes;
	
	public List<ServiceApi> getApis() {
		return apis;
	}
	public void setApis(List<ServiceApi> apis) {
		this.apis = apis;
	}
	public List<MensajeSistema> getMensajes() {
		return mensajes;
	}
	public void setMensajes(List<MensajeSistema> mensajes) {
		this.mensajes = mensajes;
	}
	
	
}
