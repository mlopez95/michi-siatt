package py.com.siatt.michi.controller;

import com.roshka.michi.exception.APIException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import py.com.siatt.michi.beans.DatosIniciales;
import py.com.siatt.michi.beans.MensajeSistema;
import py.com.siatt.michi.beans.ServiceApi;
import py.com.siatt.michi.dao.GeneralDaoImpl;

import java.util.List;


@RestController
@Api(value = "Servicios Generales", tags = "GeneralApi", description = "Aplicacion de servicios generales para los micro apis de Siatt")
public class GeneralController {

    @Autowired
    private GeneralDaoImpl generalDao;

    @ApiOperation(
            value = "Mensajes del sistema", nickname = "obtenerMensajes",
            notes = "Obtiene los mensajes para el usuario para el clientType y clientVersion dado,"
                    + " si el tipo de mensaje es FATAL o ERROR no se deberia de dejar avanzar."
                    + " Si este servicio responde con error ignorarlo",
            produces = "application/json")
    @RequestMapping(value = "/mensajes", method = RequestMethod.GET)
    public List<MensajeSistema> obtenerMensajes(

            @ApiParam(value = "Tipo de la aplicacion", required = true, example = "ANDROID")
            @RequestParam(value = "client_type", required = true) String clientType,

            @ApiParam(value = "Version de la aplicacion", required = true, example = "1.0.0")
            @RequestParam(value = "client_version", required = true) String clientVersion
    ) {

        return generalDao.obtenerMensajes(clientType, clientVersion);

    }

    @ApiOperation(
            value = "Apis Disponibles", nickname = "obtenerApis",
            notes = "Obtiene los apis para el clientType y clientVersion dado.Estos endpoints deben ser utilizados para las posteriores llamadas",
            produces = "application/json")
    @RequestMapping(value = "/apis", method = RequestMethod.GET)
    public List<ServiceApi> obtenerApis(

            @ApiParam(value = "Tipo de la aplicacion", required = true, example = "ANDROID")
            @RequestParam(value = "client_type", required = true) String clientType,

            @ApiParam(value = "Version de la aplicacion", required = true, example = "1.0.0")
            @RequestParam(value = "client_version", required = true) String clientVersion
    ) throws APIException {

        return generalDao.obtenerApis(clientType, clientVersion);
    }

    @ApiOperation(
            value = "Datos iniciales", nickname = "obtenerDatosIniciales",
            notes = "Obtiene los apis para el clientType y clientVersion dado.Estos endpoints deben ser utilizados para las posteriores llamadas"
                    + "Obtiene los mensajes para el usuario para el clientType y clientVersion dado,"
                    + " si el tipo de mensaje es FATAL o ERROR no se deberia de dejar avanzar.",
            produces = "application/json")
    @RequestMapping(value = "/datos", method = RequestMethod.GET)
    public DatosIniciales obtenerDatosIniciales(

            @ApiParam(value = "Tipo de la aplicacion", required = true)
            @RequestParam(value = "client_type", required = true) String clientType,

            @ApiParam(value = "Version de la aplicacion", required = true)
            @RequestParam(value = "client_version", required = true) String clientVersion
    ) throws APIException {

        DatosIniciales datos = new DatosIniciales();
        datos.setApis(generalDao.obtenerApis(clientType, clientVersion));
        datos.setMensajes(generalDao.obtenerMensajes(clientType, clientVersion));
        return datos;
    }

}
