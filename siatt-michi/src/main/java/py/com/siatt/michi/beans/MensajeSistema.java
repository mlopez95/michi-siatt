package py.com.siatt.michi.beans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Modelo que indica los mensajes para la pantalla inicial, antes del login")
public class MensajeSistema {

	public enum TipoMensaje{
		FATAL,
		ERROR,
		WARN,
		INFO,
		PROM;
	}
	
	private String codigo;
	@ApiModelProperty(value="El tipo del mensaje.Si es FATAL o ERROR no se debe dejar avanzar (bloquear el login)")
	private TipoMensaje tipo;
	@ApiModelProperty(value="El mensaje a mostrar al usuario")
	private String mensaje;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public TipoMensaje getTipo() {
		return tipo;
	}
	public void setTipo(TipoMensaje tipo) {
		this.tipo = tipo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
