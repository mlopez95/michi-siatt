package py.com.siatt.michi.swagger;

import org.springframework.context.annotation.Bean;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
public class SwaggerConfiguration {

	@Bean
	public Docket api() {   

		
		return new Docket(DocumentationType.SWAGGER_2)
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any()) 
          .build()
          .produces(Sets.newHashSet("application/json"))
          .globalOperationParameters(
        		  Lists.newArrayList(new ParameterBuilder()
        	            .name("X-RshkMichi-ApiKey")
        	            .description("API KEY de la aplicación solicitante.")
        	            .modelRef(new ModelRef("string"))
        	            .parameterType("header")
        	            .required(true)
        	            .build()))
          //.useDefaultResponseMessages(false)
          //.globalResponseMessage(RequestMethod.GET,errors)
	      .apiInfo(apiInfo());
	}
	
	private ApiInfo apiInfo() {
	    ApiInfo apiInfo = new ApiInfo(
	      "Michi Siatt",
	      "API que provee datos generales para el uso de los micro servicios de Siatt",
	      //"v0.0.1",
	      "v"+getClass().getPackage().getImplementationVersion(),
	      "",
	      new Contact("Roshka", "www.roshka.com", "spezzino@roshka.com"),
	      "",
	      "");
	    return apiInfo;
	}
}
